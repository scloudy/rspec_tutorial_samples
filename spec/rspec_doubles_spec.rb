class ClassRoom
    def initialize(students)
        @students = students
    end

    def list_student_names
        @students.map(&:name).join(',')
    end
end

describe ClassRoom do
    it 'the list_student_names method should work correctly' do
        student1 = double('student')
        student2 = double('student')

        allow(student1).to receive(:name) { 'John Smith' }
        allow(student2).to receive(:name) { 'Jill Smith' }

        cr = ClassRoom.new [student1, student2]
        expect(cr.list_student_names).to eq('John Smith,Jill Smith')
    end


    it 'the list_student_names method should work correctly 2' do
        student1 = double('student')
        student2 = double('student')

        allow(student1).to receive_message_chain(:name).and_return('John Smith')
        allow(student2).to receive_messages(:name => 'Jill Smith')
        
        cr = ClassRoom.new [student1, student2]
        expect(cr.list_student_names).to eq('John Smith,Jill Smith')
    end
end